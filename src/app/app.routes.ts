import { Routes } from '@angular/router';
import { ComponenteDavidComponent } from './components/componente-david/componente-david.component';
import { InicialComponent } from './components/inicial/inicial.component';
import { ValentinaComponent } from './components/valentina/valentina.component';
import { DanielaComponent } from './components/daniela/daniela.component';
import { JuanComponent } from './components/juan/juan.component';
import { SebastianComponent } from './components/sebastian/sebastian.component';

export const routes: Routes = [
    { path: '', component: InicialComponent },
    { path: 'david', component: ComponenteDavidComponent },
    { path: 'valentina', component: ValentinaComponent },
    { path: 'daniela', component: DanielaComponent },
    { path: 'juan', component: JuanComponent },
    { path: 'jorge', component: ComponenteDavidComponent },
    { path: 'sebastian', component: SebastianComponent },
    { path: 'jonathan', component: ComponenteDavidComponent },
];
