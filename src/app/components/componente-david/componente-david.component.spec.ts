import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ComponenteDavidComponent } from './componente-david.component';

describe('ComponenteDavidComponent', () => {
  let component: ComponenteDavidComponent;
  let fixture: ComponentFixture<ComponenteDavidComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ComponenteDavidComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ComponenteDavidComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
