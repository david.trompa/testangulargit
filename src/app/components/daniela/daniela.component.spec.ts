import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DanielaComponent } from './daniela.component';

describe('DanielaComponent', () => {
  let component: DanielaComponent;
  let fixture: ComponentFixture<DanielaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [DanielaComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(DanielaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
